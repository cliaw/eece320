#include <stdio.h>
#include <stdlib.h>

long power( int x, int n ); // compute x^n

int main( int argc, char** argv )
{
   printf("10^5: %ld",power(10,5));

   return 0;
}

long power( int x, int n )
{
   if( n == 0 )
      return 1;
   if( n == 1 )
      return x;
   if( n % 2 )
      return x*power(x*x,n/2);
   return power(x*x,n/2);
}
