%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All the content is in one file because I do not expect multiple editors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,A4]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% packages

\usepackage{latexsym}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[T1]{fontenc}
\usepackage{mathptmx}
\usepackage{palatino}
%\usepackage{times}
\usepackage{amsmath, amssymb}
\usepackage{fullpage}
\usepackage{complexity}
\usepackage{hyphenat}
\usepackage{multirow}
\usepackage{moreverb}
\usepackage[usenames,dvipsnames]{color}
\usepackage{boxedminipage}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{setspace}

\lstset{language=C,
        basicstyle=\footnotesize,
        numbers=left,
        numberstyle=\footnotesize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% basic definitions, Counting-notes.tex

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}{Definition}
\newtheorem{property}{Property}
\newtheorem{observation}{Observation}
\newtheorem{remark}{Remark}
\newtheorem{claim}{Claim}

\newenvironment{proof}
        {\noindent {\em Proof.}~~~} %\\
        {\begin{flushright}$\Box$\end{flushright}}

\newenvironment{indent1}
    {\begin{list}{}{
        \setlength{\leftmargin}{20mm}
        \setlength{\labelwidth}{15mm}
        \setlength{\labelsep}{5mm}}}
    {\end{list}}

\addtolength{\parskip}{0.1in}
%\addtolength{\oddsidemargin}{-0.25in}
%\addtolength{\evensidemargin}{-0.25in}
%\addtolength{\textwidth}{0.5in}
%\addtolength{\topmargin}{-.25in}
%\addtolength{\textheight}{0.75in}	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% title details

\title{
	Problem Set 2\\
        \scriptsize{\textsection
                    \ Christopher Liaw, 73118093 - Nicholas Fischer, 77157097 - Lenny Mah, 70630090
                    \textsection }
	\\ \vspace*{0.2in} \hrule
}
%\author{
%Christopher Liaw, 73118093
%\hrule
%}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\setlength{\baselineskip}{1\baselineskip}

\maketitle

\vspace*{-0.7in}

\begin{enumerate}

\item The algorithm that we will use to sort the numbers is based on an algorithm described in \textit{Introduction to Algorithms 3 ed.} by Cormen, Leiserson, Rivest, and Stein.
Our algorithm will work well provided $O(b-a) = O(n)$.
The algorithm counts the number of times a certain element occurs in an array.
Using that information, it will be able to construct a new array in sorted order.
The pseudo-code is given below.

\begin{algorithm}[ht]
\begin{algorithmic}[1]
   \STATE initialize an array C[0..size-1] to be all 0
   \FOR{ $i \leftarrow 0$ to size }
      \STATE C[data[$i$] - min] $\leftarrow$ C[data[$i$] - min] + 1
      \STATE $i \leftarrow i + 1$
   \ENDFOR
   \STATE $k \leftarrow 0$
   \FOR{ $i \leftarrow 0$ to max - min }
      \FOR{ $j \leftarrow 0$ to C[$i$] }
         \STATE data[$k$] $\leftarrow i$ + min
         \STATE $k \leftarrow k + 1$
      \ENDFOR
   \ENDFOR
\end{algorithmic}
\caption{$\mbox{\sc CountingSort}$(data,size,min,max)}
\end{algorithm}

\textbf{Proof of correctness.} For the first loop, we need to prove the following invariant is true.\\
\textit{Invariant.} After $k$ iterations, the identity, $\sum_j C[j] = k$ holds.\\
\textit{Proof.} The invariant is saying that if we have looked in $k$ elements in our input, then we have successfully counted $k$ inputs. Initially, this is true. After the first iteration, $\sum_j C[j] = 1$, so the invariant holds.
Before the $k$th iteration of the loop, the sum is $k-1$. The algorithm will add one more the appropriate $C[j]$. Hence, the sum is $k$.

The next invariant is for the second loop.\\
\textit{Invariant.} $\sum_{n=0}^j C[n] + \sum_{n=j+1}^{min+max} C[n] = size$.\\
\textit{Proof.} The invariant is saying that we will not "lose" any elements from the original first array, since the sum of all the counts should be constant.
Initially, this is trivially true due to the correctness of the first loop.
Suppose after $i$ iterations, the invariant is still true.
Then we place $C[i]$ values of $i + \mbox{min}$ into data.
Hence, the number of values we have added to data increments by $C[i]$, and we have $C[i]$ less elements to deal with.
The invariant is preserved and this concludes the proof.

\textbf{Asymptotic complexity.} The first for loop looks at $n$ elements of the input array, where $n$ is the size of the array.
This takes $O(n)$ time.
The second loop looks at each element in C and writes to data whenever it needs to.
This takes $O(n + b-a)$ time.
If $O(b-a) = O(n)$, then this algorithm is bounded by $O(n)$ in the worst case.
Since we must look at all elements at least once, it is bounded by $\Omega(n)$ in the best case.
Hence, we have a $\Theta(n)$ bound on this algorithm.

\item The idea behind the algorithm is straightforward.
We will check to see if $c = a^b$ by checking if an integer, $a$ exists, such that for some $b$, $a^b = c$.

A high level description of the algorithm is as follows:
\begin{enumerate}[(i)]
\item Initialize $b = 2$.
\item Determine if there exists an element $a \in \left[ 0,\left\lceil c^{\frac{1}{b-1}}\right\rceil \right]$ such that $a^b = c$.
\item Increment $b$ by one and repeat (ii) until $2^b > c$.
\end{enumerate}
Note that since $c$ is an $n$-bit number, the equality, $c < 2^n$ will hold.

We will use the method to compute exponentiation as discussed in class which takes $O(n\lg n)$ time.
In the pseudo-code, we will denote $a^b$, by $\mbox{\sc Pow}(a,b)$.
In order to calculate $\left\lceil \sqrt[n]{x} \right\rceil$, we use a slightly modified binary search.
The pseudo-code is given below to illustrate the difference.
However, we omit the analysis, since there is little difference between our modified binary search and the binary search discussed in first and second year courses.

\begin{algorithm}[ht]
\begin{algorithmic}[1]
   \STATE $mid \leftarrow (max+min)/2$
   \IF{ $c = \mbox{\sc Pow}(mid,n)$ or $(c < \mbox{\sc Pow}(mid-1,n)$ and $c > \mbox{\sc Pow}(mid,n))$}
		\RETURN $mid$
	\ENDIF
	\IF{ $c > \mbox{\sc Pow}(mid,n)$}
		\RETURN $\mbox{\sc RootSearch}(mid+1,max,n,c)$
	\ENDIF
	\RETURN $\mbox{\sc RootSearch}(min,mid-1,n,c)$
\end{algorithmic}
\caption{$\mbox{\sc RootSearch}(min,max,n,c)$}
\end{algorithm}

Now that we have two functions to help with everything, the original problem is simple.
The pseudo-code is given below.
We use the notation $1 << b$ to represent a shift 1 over by $b$ bits.
This is to differentiate between the $\mbox{\sc Pow}$ function.

\begin{algorithm}[ht]
\begin{algorithmic}[1]
	\STATE $a \leftarrow c$
	\STATE $b \leftarrow 2$
	\WHILE{ $(1 << b) <= c$ }
		\STATE $a \leftarrow \mbox{\sc RootSearch}(0,a,b,c)$
		\IF{ $\mbox{\sc Pow}(a,b) = c$ }
			\RETURN true
		\ENDIF
		\STATE $b \leftarrow b+1$
	\ENDWHILE
	\RETURN false
\end{algorithmic}
\caption{An algorithm to determine if $c = a^b$}
\end{algorithm}

\textbf{Proof of correctness.} The algorithm will terminate if one of two conditions hold:
\begin{itemize}
\item we found $b,a$ such that $a^b = c$ and we return true; or
\item $b \ge n$ and so $2^b \ge c$ and we fail the while condition and exit the loop to return false.
\end{itemize}
Hence, we just need to prove the following invariant.\\
\textit{Invariant.} At the end of each iteration of the loop (before we increment $b$), either we have returned true or $(a-1)^{b}<c<a^b$.\\
\textit{Proof.} The $\mbox{\sc RootSearch}$ function gives us the smallest integer, $a$, such that $a^b \ge c$.
Initially, $b=2$, so either $a^2 = c$ and we return true or $a^2 \ge c$.
For the next iteration of the loop, we will search in the interval, $[0,a = \lceil\sqrt{c}\rceil]$ for $\sqrt[3]{c}$.
After $k$ iterations, $b=k+1$, so either $a^{k+1} = c$ and we return true or $a^{k+1} \ge c$.
If $2^{k+2} \le c$, then in the next iteration of the loop, we will search inside the interval, $[0,a=\lceil\sqrt[k+1]{c}\rceil]$ for $\sqrt[k+2]{c}$.
This is fine, since we know that $\sqrt[k+2]{c} \in [0,\sqrt[k+1]{c}]$.
\\

\textbf{Asymptotic complexity.} To study the asymptotic complexity of the algorithm, we will look at the number of multiplications since all the other operations take constant time.
In the $i$th iteration of the while loop, we do a binary search looking for $\left\lceil \sqrt[i]{c} \right\rceil$.
This requires at most, $\lg(\sqrt[i-1]{c})\lg(i)$ multiplications.
The term $\lg(\sqrt[i-1]{c})$ corresponds to how many searches we will have to perform, at most, to find $\left\lceil \sqrt[i]{c} \right\rceil$.
The second term, $\lg(i)$, represents the number of multiplications needed to check if we have found the element we are searching for.
The outer loop runs $n$ times.
Letting $M(n)$ be the number of multiplications, we get the following:
\begin{equation}\label{eq:q2_1}
M(n) = \sum_{i=2}^{n-1} \lg (\sqrt[i]{c} )\lg (i+1) = \lg(c)\sum_{i=2}^{n-1}\frac{\lg(i+1)}{i}.
\end{equation}
The summation on the right hand side of equation (\ref{eq:q2_1}) is hard to evaluate.
However, we can treat the sum as the approximation to an integral of a similar function where we take the right end of the partition of size 1.\footnote{Of course, we can just throw away the $i$ in the denominator and replace $\lg(2i)$ with $\lg(2n)$, but doing this gives a much better upper bound.}
Figure 1 is used to illustrate the point.
\begin{figure}[ht]
	\centering
	\includegraphics[width=4in]{q2.png}
	\caption{A Riemann approximation to the integral, $\int_2^n \frac{\log(2x)}{2x}\,dx$. Source: http://science.kennesaw.edu/~plaval/applets/Riemann.html}
\end{figure}
Mathematically,
\begin{eqnarray*}
\lg(c)\sum_{i=2}^{n-1}\frac{\lg(i+1)}{i} & \le & 2\lg(c)\sum_{i=2}^{n-1}\frac{\lg(2i)}{2i}\\
& \le & 2\lg(c)\frac{\lg(4)}{4} + 2\lg(c)\int_2^n \frac{\ln(2x)}{2x\ln(2)}\,dx\\
& \le & k_1\lg(c) + k_2\lg(c)\lg^2(2n),
\end{eqnarray*}
where $k_1, k_2$ are appropriate constants. The integration was done by substitution.
Also $\lg(c) \le n$ since $c$ is an $n$-bit number.
Hence, we have $M(n) \in O(n\lg^2(n))$. Adding in the time to do $n$-bit multiplications, we have
\[
T(n) \in O(n^3\lg^2(n)) \mbox{ or } T(c) \in O(\lg^3(c)\lg^2(\lg(c))).
\]



%%%%%%%%%%%%
\item Let us say that an ordered pair of bids, $(a_i,b_i)$ is greater than another pair of bids, $(a_j,b_j)$ if and only if the following conditions hold:
\begin{itemize}
\item $a_i > a_j$; or
\item $a_i = a_j$ and $b_i > b_j$.
\end{itemize}
If $a_i = a_j$ and $b_i = b_j$, we say the pair of bids are equal.

The algorithm has the following procedure:
\begin{enumerate}[(i)]
\item Sort all the bids in increasing order of $(a_i, b_i)$.
\item For each bid, $(a_i, b_i)$, determine if it is a dominated bid and push them into the proper stack (either stack of dominated bids or stack of non-dominated bids).
\end{enumerate}

In order to do the second step, we begin with the bid $(a_n, b_n)$.
Clearly, this bid cannot be dominated, because for all $i < n$, $a_i \le a_n$.
Let $b_{{max}} = \max\{ b_i,b_{i+1},..,b_n \}$.
Let $a_{{max}}$ be the other value in the ordered pair.
In other words, as we move down the list, we remember the pair with the largest bid for the Picasso painting that we find.
If a pair of bids, $(a_i,b_i)$ satisfy $a_i < a_{{max}}$ and $b_i < b_{{max}}$, then it is clearly dominated.

In the pseudo-code below, we use bids to represent an array consisting of all the bids that have been cast.
The variable, dominatedStack refers to the stack of dominated bids.
The variable, nondominatedStack refers to the stack of non-dominated bids.

\begin{algorithm}[ht]
\begin{algorithmic}[1]
	\STATE create two empty stacks, dominatedStack and nonDominatedStack
	\STATE MergeSort( bids )
	\STATE $a_{{max}} \leftarrow 0$
	\STATE $b_{{max}} \leftarrow 0$
	\FOR {$i \leftarrow n$ to $1$}
		\IF {$b_{{max}} < b_i$}
			\STATE $a_{{max}} \leftarrow a_i$
			\STATE $b_{{max}} \leftarrow b_i$
		\ENDIF
		\IF {$a_{{max}} > a_i$ and $b_{{max}} > b_i$}
			\STATE push bids[$i$] to dominatedStack			
			\STATE continue
		\ENDIF
		\STATE push bids[$i$] to nondominatedStack
	\ENDFOR
	\RETURN nondominatedStack
\end{algorithmic}
\caption{$\mbox{\sc GetNonDominatedBids}$(bids)}
\end{algorithm}
\textbf{Proof of correctness.} Merge sort has been described in class and we will assume that after merge sort has been implemented, the array of bids, has been sorted.
This is fine, because we have defined an order for bids above.
We now need to prove the correctness of the loop.
We will do this by proving the following invariant.

\textbf{Invariant:} Upon entrance into the loop for $(n-i+1)$th iteration, the stack of dominated bids will contain all dominated bids in the sub-array, bids[$i+1..n$].
In addition, the stack of non-dominated bids will contain all non-dominated bids in the sub-array, bids[$i+1..n$].
Equivalently, after $i$ iterations of the loop, the stack of dominated bids will contain all dominated bids in the sub-array, bids[$i..n$] and the stack of non-dominated bids will contain all non-dominated bids in the sub-array, bids[$i..n$].
Finally, at any time, $b_{max} \ge b_j$ for $j \in [i,n]$.\\
\textbf{Proof.} Upon initialization, both stacks are empty.
At the end of the first loop, $(a_n,b_n)$ will be in the stack of non-dominated bids.
In addition, $(a_{max},b_{max}) = (a_n,b_n)$.
So the invariant holds after the first iteration.

On the $i$th iteration, we know that $(a_{max},b_{max}) \ge (a_i,b_i)$, since we know it is sorted.
Therefore, one of the following is true:
\begin{enumerate}[(i)]
\item $a_{max} = a_i$ and $b_{max} \ge b_i$;
\item $a_{max} > a_i$ and $b_{max} > b_i$;
\item $a_{max} > a_i$ and $b_{max} \le b_i$.
\end{enumerate}
In case (i), $(a_i,b_i)$ is not dominated because $a_{max} = a_i$. We add this to the stack of non-dominated bids.
In case (ii), $(a_i,b_i)$ is dominated by definition, so we add it to the stack of dominated bids.
In case (iii), $(a_i,b_i)$ is not dominated because $b_i \ge b_{max}$, so we add it the stack of non-dominated bids.
If $b_{max} < b_i$, then let $(a_{max}, b_{max}) = (a_i,b_i)$, so that $b_{max} = \max{\{b_i,..,b_n\}}$.
We have shown that the invariant holds after $n-i+1$ iterations and this concludes the proof.

\textbf{Asymptotic bound.} The derivation for the bound is straightforward.
Merge sort runs in $\Theta(n\lg n)$ time.
Next, we look through all elements once and put them in their respective stack.
This takes $\Theta(n)$ time.
Hence, the algorithm runs in $\Theta(n) + \Theta(n\lg n) = \Theta(n\lg n)$ time.

%%%%%%%%%%%%%%
%% Question 4
%%%%%%%%%%%%%%
\item The technique that we are going to propose to find the median
\footnote{We will define the median as follows:
\begin{itemize}
\item if there are an odd number of elements, the median is the middle element; or
\item if there are an even number of elements, the median is the average of the two elements in the middle.
\end{itemize}
}
of three sorted lists can be generalized to $k$ sorted lists, provided that there are an equal number of values above and below the median in all the lists combined.
The fact that the list are all of size $n$ guarantees this.

A high level description for $k$ lists of size $n$ is given below, but the rest of the analysis will fix $k=3$.
\begin{enumerate}[(i)]
\item Set $i=k$, where $i$ is the number of non-empty lists.
\item Obtain all $i$ medians. For the list with the smallest median, obtain the number of values that are to the left of the median. Call this $a$. For the list with the largest median, obtain the number of values to the right of the median. Call this $b$. Throw away $\min(a,b)$ of the smallest values from the first list and $\min(a,b)$ of the largest values from the second list.
\item If the smallest or largest median comes from a list of size 1, destroy the list and throw away one element from the other list that we compared (this may end up destroying both lists). The element that is thrown away should be either the largest or smallest in the list, respectively. Whenever a list is destroyed, decrement $i$ by one or two (depending on how many are destroyed. Repeat (ii) until there is one list left or two lists with one element in each. The median of the union of the lists is the median of the sole surviving list or the average of two single-element lists.
\end{enumerate}
We need to be careful with step (ii) and (iii).
Step (ii) says to only throw away elements that are \textit{strictly} less than or greater than the median.
If $a$ is the size of the list, then the number of elements would be $\frac{1}{2}(a-1)$.
This ensures we do not throw away the median.
Step (iii) investigates the corner case in which we are comparing two lists of size 1.
In addition, if we have two lists, both of size 1, then we should break out of the loop and return the average of the two remaining items.
For example, if $A=\{1\}$ and $B=\{3\}$, then return 2.

The pseudo-code is presented below. It makes no assumption on the number of lists.
Instead of throwing out the elements, we will use iterators.
In the pseudo-code, we use the notation list.right() and list.left() to denote the right and left iterators, respectively.
\begin{algorithm}[ht]
\begin{spacing}{1.0}
\begin{algorithmic}[1]
	\STATE done $\leftarrow$ FALSE
	\STATE median $\leftarrow$ INF
	\STATE initialize left iterators to 0
	\STATE initialize right iterators to the size of the list - 1
	\WHILE{ done is false }
	  \STATE // corner case
	  \IF{ two non-empty lists remain with one element each }
	  	\RETURN average of two remaining elements
	  \ENDIF
      \STATE X $\leftarrow$ non-empty list with largest median
      \STATE Y $\leftarrow$ non-empty list with smallest median
      \STATE delta $\leftarrow$ floor(min(length(X) - 1,length(Y) - 1)/2)
      \STATE delta $\leftarrow$ max(delta,1) // throw away at least one thing
		\STATE X.right() $\leftarrow$ X.right() - delta
      \STATE Y.left() $\leftarrow$ Y.left() + delta
      \FOR{ all non-empty lists }
         \IF{ left iterator > right iterator }
         	\STATE set list to empty
         \ENDIF
      \ENDFOR
		\IF {all but one list is empty (empty means left iterator > right iterator)}
			\STATE done $\leftarrow$ TRUE
		\ENDIF
	\ENDWHILE
	\FOR{ each list }
		\IF{ list is not empty }
			\RETURN median(list)
			\STATE //(list[ceil((list.right()-list.left())/2)] + list[floor((list.right()-list.left())/2)])/2
		\ENDIF
	\ENDFOR
\end{algorithmic}
\end{spacing}
\caption{$\mbox{\sc MedianSearch}(A,B,C)$}
\end{algorithm}

\textbf{Proof of correctness.} The algorithm terminates, since we are halving the size of two lists after each iteration.
Eventually, at least two of them will be empty, so the variable, done, will be set to true and the condition inside the while fails.
To prove that the algorithm is correct, we need to prove a single invariant.\\
\textit{Invariant.} There are always an equal number of elements greater than the median and less than the median in the union of the three lists.\\
\textit{Proof.} This is trivially true upon initialization since we have not even touched the lists.
After the first iteration is finished, we throw away an equal number of elements greater than the median and less than the median.
Hence, the invariants holds.

For every iteration thereafter, we need to consider multiple cases:
\begin{itemize}
\item If there is one list left, then by the invariant, the median is in the middle prior to the test in the while loop.
The condition in the while loop will fail and we have the median.
\item If there are two lists left with one element each and the invariant still holds before entry into the loop, then the invariant will hold after the loop, since we will have returned the average of two elements as the median.
\item If there are more than two lists left and the largest or smallest median comes from a list with one element, then we throw away one element.
This will destroy at least one list, but the invariant still holds since we threw away one element greater than or equal to the median and one element less than or equal to the median.
\end{itemize}
we throw away an equal number of elements in the list containing the largest and smallest element.
Hence, the invariant holds.
This concludes the proof.

\textbf{Asymptotic complexity.} After each iteration of the loop, the length of two lists will be halved.
To halve one list of size $n$ to a single element requires $\lg n$ iterations.
Hence, the worst case is bounded by $3\lg n \in O(\lg n)$ iterations of the loop.
The algorithm does require that we halve at least one list.
This requires $\lg n \in \Omega(\lg n)$ iterations of the loop.
Most operations in the algorithm takes constant time, with the longest possibly being the additions (the division will not take that long, since it is a single bit shift to the right).
Assuming the additions take constant time, we have a bound of $\Theta(\lg n)$.

\end{enumerate}

\end{document}
