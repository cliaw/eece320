#include <stdio.h>
#include <stdlib.h>

// Define some macros
#define mid(a,b) ( (a+b)/2 )
#define min(a,b) (a < b ? a : b)
#define max(a,b) (a > b ? a : b)
#define half(a,b) ((max(a,b)-min(a,b)+1)/2)
#define threeOf(a,b,c) (a&&b&&c)
#define twoOf(a,b,c) ( (a&&b) || (b&&c) || (a&&c) )
#define oneOf(a,b,c) ( a || b || c )

typedef enum {
   FALSE,
   TRUE
} bool_t;

int main( int argc, char** argv )
{
   int B[] = {1, 3, 4, 5, 6, 6, 8, 12,13,15,17,1000,1010};
   int A[] = {18,19,20,21,22,23,24,25,26,27,28,90,400};
   int C[] = {89,90,91,92,93,94,95,96,97,98,99,100,300};
   size_t leftA = 0, leftB = 0, leftC = 0;
   size_t rightA = sizeof(A)/sizeof(int) - 1, rightB = sizeof(B)/sizeof(int) - 1, rightC = sizeof(C)/sizeof(int) - 1;
   size_t midA, midB, midC;
   int tmpA, tmpB, tmpC;
   size_t delta;
   int median = 0;
   bool_t bEmptyA = FALSE, bEmptyB = FALSE, bEmptyC = FALSE;
   
   while(twoOf(half(leftA,rightA),half(leftB,rightB),half(leftC,rightC))) {
      midA = mid(leftA,rightA);
      midB = mid(leftB,rightB);
      midC = mid(leftC,rightC);
      
      tmpA = A[midA];
      tmpB = B[midB];
      tmpC = C[midC];
      
      bEmptyA = leftA == rightA;
      bEmptyB = leftB == rightB;
      bEmptyC = leftC == rightC;
      
      if(!bEmptyA && (min(tmpA,tmpB) == tmpA || bEmptyB) && (min(tmpA,tmpC) == tmpA || bEmptyC)) // tmpA < tmpC and tmpA < tmpB
      {
         if(bEmptyB || min(tmpB,tmpC) == tmpB) // tmpB < tmpC
         {
            delta = min(half(leftA,rightA),half(leftC,rightC));
            leftA += delta;
            rightC -= delta;
         } else if(bEmptyC || min(tmpB,tmpC) == tmpC) { // tmpC < tmpB
            delta = min(half(leftA,rightA),half(leftB,rightB));
            leftA += delta;
            rightB -= delta;
         }
      }
      else if(!bEmptyB && (min(tmpB,tmpA) == tmpB || bEmptyA) && (min(tmpB,tmpC) == tmpB || bEmptyC)) // tmpB < tmpA and tmpB < tmpC
      {
         if(bEmptyA || min(tmpA,tmpC) == tmpA)
         {
            delta = min(half(leftB,rightB),half(leftC,rightC));
            leftB += delta;
            rightC -= delta;
         } else if(bEmptyC || min(tmpA,tmpC) == tmpC) {
            delta = min(half(leftB,rightB),half(leftA,rightA));
            leftB += delta;
            rightA -= delta;
         }
      }
      else if(!bEmptyC && (min(tmpC,tmpA) == tmpC || bEmptyA) && (min(tmpC,tmpB) == tmpC || bEmptyB)) // tmpC < tmpA and tmpC < tmpB
      {
         if(bEmptyA || min(tmpA,tmpB) == tmpA)
         {
            delta = min(half(leftB,rightB),half(leftC,rightC));
            leftC += delta;
            rightB -= delta;
         } else if(bEmptyB || min(tmpA,tmpB) == tmpB) {
            delta = min(half(leftA,rightA),half(leftC,rightC));
            leftC += delta;
            rightA -= delta;
         }
      }
      
      printf( "leftA: %d rightA %d\n",leftA,rightA);
      printf( "A: " );
      for(tmpA = leftA; tmpA <= rightA; tmpA++)
      {
         printf("%d ",A[tmpA]);
      }
      printf( "\n" );
      printf( "leftB: %d rightB %d\n",leftB,rightB);
      printf( "B: " );
      for(tmpB = leftB; tmpB <= rightB; tmpB++)
      {
         printf("%d ",B[tmpB]);
      }
      printf( "\n" );
      printf( "leftC: %d rightC %d\n",leftC,rightC);
      printf( "C: " );
      for(tmpC = leftC; tmpC <= rightC; tmpC++)
      {
         printf("%d ",C[tmpC]);
      }
      printf( "\n\n" );
   
   }
   
   if(!bEmptyA)
      median = A[mid(leftA,rightA)+1];
   else if(!bEmptyB)
      median = B[mid(leftB,rightB)+1];
   else if(!bEmptyC)
      median = C[mid(leftC,rightC)+1];
   
   printf( "Median: %d\n", median );
   
   return 0;
}
