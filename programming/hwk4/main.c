#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <limits.h>

#define DBG 0
#if DBG
# define LOG(...) fprintf(stderr,__VA_ARGS__)
#else
# define LOG(...)
#endif

#define UINF UINT_MAX
#define SINF INT_MAX
#define INF UINF

/*
** Min/Max macros
*/
#define min(a,b) (a < b ? a : b)
#define max(a,b) (a > b ? a : b)

/*
** The memo struct that we will be using.
** line - current line number
** words - number of words currently on the line
** cost - the minimum cost of the paragraph
*/
typedef struct {
   unsigned int line;
   unsigned int words;
   unsigned int cost;
} memo_t;

/*
** Function: memoize
** \param[in]: memo - the current array of memos
** \param[in]: totWords - the number of words currently in the array of memos
**                      - size of memo array is totWords+1 (one dummy 0 elem)
** \param[in]: lineLen - the maximum length of each line
** \param[in]: text - the array of text to add
** \RETURN: a memo_t struct containing the line, words, and cost
** Description: This is the function to do the memoization. The algorithm
** is described in the homework. In summary, it loops through all possibilities
** and tries to find the minimum cost, where cost = (max line length - words on line)^2.
** Note: memo should not be empty and should be of size at least totWords+1.
**       text should be of size at least totWords
**       Otherwise, memoize could result in a seg fault.
*/
memo_t memoize( memo_t *memo, int totWords, int lineLen, char **text );

int main( int argc, char** argv )
{
   char* fname = NULL;
   char str[1 << 10] = {0};
   FILE *pFile;
   char **text = NULL;
   unsigned int numWords = 0;
   unsigned int i = 0,j,k;
   unsigned int lineLen = 30;
   memo_t *memo = NULL;
   int* wordsPerLine = NULL;

   /*
   ** Arguments are as follows:
   ** 1. File name
   ** 2. Maximum length of line
   */
   fname = argv[1];
   lineLen = atoi(argv[2]);
   pFile = fopen(fname,"r");
   if( !pFile ) {
      fprintf( stderr, "Failed to open %s. Asserting\n",fname );
      assert(pFile != NULL);
   }
   
   /*
   ** Run through each word in the supplied file until we hit EOF.
   ** Keep track of the number of words as we go, since we will
   ** be using the information later.
   */
   while(fscanf(pFile,"%s",str) != EOF) {
      numWords++;
      text = realloc( text, sizeof(char*) * numWords );
      text[numWords-1] = malloc(sizeof(char) * (strlen(str)+1));
      memcpy(text[numWords-1],str,(strlen(str)+1)*sizeof(char));
      //memset(str,0,sizeof(str));

      /*
      ** Some debuggin.
      */
      LOG( "%d ", numWords );
      LOG( "String: %s ", str );
      LOG( "%s \n", text[numWords-1]);
   }
   
   memo = malloc(sizeof(memo_t)*(numWords+1));
   memset(memo,0,sizeof(memo_t)*(numWords+1));
   
   /*
   ** Run the algorithm to figure out the minimum cost after
   ** inserting each word.
   */
   for( i = 1; i <= numWords; i++ )
      memo[i] = memoize( memo, i-1, lineLen, text );

   /*
   ** Memoize stores the number of words in the current line
   ** that minimizes the total cost.
   ** With that information, it is not very had to backtrack
   ** and figure it out how many words needs to get printed
   ** on each line.
   ** In higher level languages, the array would probably be 
   ** a stack, but it is much less work to just use an array
   ** for this little task. We will just fill it up backwards
   ** and access it forwards later.
   */

   /*
   ** We will use j to keep track of what line we are on.
   ** And use i to keep track of how many words are left
   ** to scan through.
   */

   j = memo[numWords].line;
   wordsPerLine = malloc(j*sizeof(int));
   i = numWords;
   while( i ) {
      wordsPerLine[--j] = memo[i].words;
      i -= memo[i].words;
   }

   /*
   ** We should now know how many words are on each line.
   **/ 
   k = 0;
   for( i = 0; i < memo[numWords].line; i++ )
   {
      for( j = 0; j < wordsPerLine[i]-1; j++ )
      {
         fprintf(stdout, "%s ", text[k++] );
      }
      fprintf(stdout, "%s\n", text[k++] );
   }

   /*
   ** Clean up all the pointers.
   */
   for( i = 0; i < numWords; i++ )
   {
      free(text[i]);
      text[i] = NULL;
   }
   free(text);
   free(memo);
   free(wordsPerLine);
   text = NULL;
   memo = NULL;
   wordsPerLine = NULL;

   return 0;
}

/*
** Function: memoize
** \param[in]: memo - the current array of memos
** \param[in]: totWords - the number of words currently in the array of memos
**                      - size of memo array is totWords+1 (one dummy 0 elem)
** \param[in]: lineLen - the maximum length of each line
** \param[in]: text - the array of text to add
** \RETURN: a memo_t struct containing the line, words, and cost
** Description: This is the function to do the memoization. The algorithm
** is described in the homework. In summary, it loops through all possibilities
** and tries to find the minimum cost, where cost = (max line length - words on line)^2.
** Note: memo should not be empty and should be of size at least totWords+1.
**       text should be of size at least totWords
**       Otherwise, memoize could result in a seg fault.
*/
memo_t memoize( memo_t *memo, int totWords, int linelen, char **text )
{
   memo_t tmp;
   int waste = 0;
   int i, j;

   /*
   ** Default the minimum cost to INF.
   ** Line should be at least one.
   ** Very first element in memo should be a dummy.
   */
   tmp.cost = INF;
   tmp.line = (totWords > 0 ? memo[totWords].line : 1);

   /*
   ** Find out how many words are in the current line that we are looking at
   ** before optimization. We can do this by looking at the last element
   ** in the memo and then finding out when we needed to go to that line.
   ** Then, determine if we can our next word onto that line.
   ** If we cannot, start a newline.
   */
   i = totWords;
   while( i-- > 0 && memo[i].line >= memo[totWords].line );
   tmp.words = totWords - max(i,0) + 1;
   LOG( "i = %d, line = %d, numWords = %d\n", i, tmp.line, tmp.words );   
   i = totWords - tmp.words + 1;
   waste = linelen;
   for( j = i; j <= totWords; j++ )
   {
      LOG( "j= %d\n", j );
      LOG( "Testing: Waste = %d j = %d text = %s length = %d\n", waste, j, text[j], strlen(text[j]) );
      waste -= strlen(text[j]) + 1;
   }
   if(waste + 1 < 0)
   {
      tmp.words = 1;
      tmp.line++;
   }

   /*
   ** Now we know what line we are on.
   ** We are really calculating the next value for the memo,
   ** and hence should have all previous values handy.
   ** Exhaustively check all possible combinations to achieve
   ** the lowest possible cost.
   ** First, set i to point to the first word on the current line
   ** and calculate cost.
   ** Then, decrement i and repeat above.
   ** When the line gets too long, break out of the loop.
   */
   i = totWords - tmp.words + 1;
   do
   {
      waste = linelen;
      for( j = i; j < totWords; j++ )
         waste -= strlen(text[j]) + 1;
      waste -= strlen(text[j]);
      if(waste < 0) break;
      if(tmp.cost > waste*waste + memo[i > 0 ? i : 0].cost)
      {
         tmp.cost = waste*waste + memo[i > 0 ? i : 0].cost;
         tmp.words = j-i + 1;
      }
      i--;
   } while( i > 0 );

   LOG( "Returning memo: line: %u, words: %u, cost: %u",
        tmp.line, tmp.words,tmp.cost );
   return tmp;
}
