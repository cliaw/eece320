/******************************************************
* File: randomize.c
* Implementation file for random permutation functions
******************************************************/
#include <time.h>
#include "randomize.h"

#define random(i,n) (rand() % (n-i) + i)

/**************************************************************************
** FUNCTION randomizeInPlace
** \param[inout]: data - on input: pointer to an array of ints with
**                       integers
**                     - on output: pointer to an array of ints with
**                       integers in the range [0,size-1] in random order
** \param[in]: size - size of data
** RETURN: n/a
** This algorithm was taken from Introduction to Algorithms 3e by Cormen,
** Leiserson, Rivest, and Stein.
***************************************************************************/
void randomizeInPlace( INOUT int* data,
                       IN size_t size )
{
   int i  = 0;
   static bool_t initSeed = FALSE;
   
   // I think time returns something which is given in seconds
   // so it's probably better to initialize the seed only once
   if(!initSeed) {
      srand(time(NULL));
      initSeed = TRUE;
   }
   for( i = 0; i < size; i++ ) {
      swap(&data[i],&data[random(i,size)]);
   }
}
