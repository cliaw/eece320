/******************************************************
* File: main.c
* Entry point for running the sorting algorithms
******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "common.h"
#include "randomize.h"
#if HAVE_BUBBLESORT_H
# include "bubbleSort.h"
#endif
#if HAVE_MERGESORT_H
# include "mergeSort.h"
#endif
#if HAVE_QUICKSORT_H
# include "quickSort.h"
#endif

typedef void(*func_ptr)(int*,int,int);
/*****************************************************************
** You may replace your sorting algorithm here.
** Main will loop through all of them one at a time.
** Comment out any sorting algorithm that you do not wish to use.
*****************************************************************/
static struct sortAlgo
{
   char* name;
   func_ptr sort;
} sortAlgos[] =
{
   { "BubbleSort", bubbleSort},
   { "MergeSort", mergeSort},
   { "QuickSort", quickSort}
};

typedef enum eSORTING_ALGO
{
   eEXIT = -1,
   eBUBBLE_SORT = 0,
   eMERGE_SORT,
   eQUICK_SORT
} eSORTING_ALGO;

void runSorting( IN struct sortAlgo* algo,
                 IN size_t size,
                 IN int iter );

// START main..
int main( int argc, char** argv ) {
   size_t size;
   int iter = 0;
   int numSortAlgos = sizeof(sortAlgos)/sizeof(sortAlgos[0]);
   eSORTING_ALGO eSortingAlgo;
   if(DBG) printf( "numSortAlgos: %d\n", numSortAlgos );
   
   while(1) {
      printf( "Enter sorting algorithm. Select one of: \n"
        "   [0] Bubble Sort\n"
        "   [1] Merge Sort\n"
        "   [2] Quick Sort\n"
        "   [-1] to exit\n" );
      scanf( "%d", (int *)&eSortingAlgo );
      switch( eSortingAlgo ) {
         case eEXIT:
            return 0;
         case eBUBBLE_SORT:
            printf( "Bubble Sort selected.\n" );
            break;
         case eMERGE_SORT:
            printf( "Merge Sort selected.\n" );
            break;
         case eQUICK_SORT:
            printf( "Quick Sort selected.\n" );
            break;
         default:
            printf( "Error! Select a sorting algorithm" );
            continue;
            break;
      }
      printf( "Enter size: " );
      scanf( "%ld", &size );
      if( !size ) {
         printf( "Size should be positive.\n" );
         continue;
      }
      printf( "Enter number of iterations: " );
      scanf( "%d", &iter );
      if( !iter ) {
         printf( "The number of iterations should be positive.\n" );
         continue;
      }
      
      runSorting( &sortAlgos[eSortingAlgo], size, iter );
      printf( "Done!\n\n" );
   }
   return 0;
}

/*******************************************************************************
** FUNCTION runSorting
** \param[in]: algo - pointer to the structure containing the sorting algorithm
**                  - the struct contains the name of the algorithm, and a
**                    pointer to the function
** \param[in]: size - size of array that we want to sort
** \param[in]: iter - number of iterations
** RETURN: n/a
** This function will run the sorting algorithm defined in algo on an input of
** the size as given. It will run it for iter iterations.
** It will output the results (time to sort) to a file.
** For example, if we are running BubbleSort on an array of size 1000, then
** a file will be created called BubbleSort_1000.txt, where each line contains
** the time to sort an array of size 1000 (in microseconds), once.
*******************************************************************************/
void runSorting( IN struct sortAlgo* algo,
                 IN size_t size,
                 IN int iter )
{
   int *data = NULL;
   int i;
   int n = 0;
   struct timespec start, end;
   char str[200];
   int timeTakenUs;
   FILE* fp;
   
   if( !size ) return;
   
   for( n = 0; n < iter; n++ ) {
      data = (int *)malloc( sizeof(int) * size );
      if( data == NULL ) {
         fprintf( stderr, "Memory allocation error..." );
         return;
      }
      
      // Fill in the array
      for(i = 0; i < size; i++) {
         data[i] = i;
      }
      
      // shuffle the array
      randomizeInPlace( (int *)data, size );
      
      clock_gettime(CLOCK_MONOTONIC, &start); // start timer
      algo->sort(data,0,size-1); // run the sorting algorithm
      clock_gettime(CLOCK_MONOTONIC, &end); // end timer
      
      // get the time taken, in microseconds
      timeTakenUs = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
      sprintf( str, "%s_%ld.txt", algo->name, size );
      
      // open a file for writing, then close it
      fp = fopen(str, "a" );
      if( fp ) {
         fprintf(fp, "%d\n", timeTakenUs);
         fclose(fp);
      } else {
         printf( "Error opening file %s!\n", str );
      }
      
      // for debugging purposes
#if DBG
      printf( "Time taken for %s: %d us\n", str, timeTakenUs );
      printf( "data: " );
      for( i = 0; i < size; i++ ) {
         printf( "%d ", data[i] );
         if( i && !(i%20) ) printf( "\n" );
      }
      printf( "\n" );
#endif


      free(data);
      data = NULL;
   }
}
