/**********************************************
* File: bubbleSort.c
* Implementation file for bubble sort
***********************************************/
#include "bubbleSort.h"

/**************************************************************************
** FUNCTION bubbleSort
** \param[inout]: data - pointer to an array of integers
**                     - outputs sorted array
** \param[in]: start - index to start sorting at
** \param[in]: end - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void bubbleSort( INOUT int* data,
                 IN int start,
                 IN int end )
{
   int i = start;
   int j = 0;
   int size = end + 1;
   long long n = 0;
   bool_t bSwapped;

   for( i = 0; i < size - 1; i++ ) {
      bSwapped = FALSE;
      for( j = 0; j < size - i - 1; j++ ) {
         if( data[j+start] > data[j+1+start] ) {
            n++;
            bSwapped = TRUE;
            swap(&data[j+start],&data[j+start+1]);
         }
      }
      if(!bSwapped) break;
   }
   if(DBG) printf( "N = %lld\n",n );
}