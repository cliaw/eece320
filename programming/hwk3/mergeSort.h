/**********************************************
* File: mergeSort.h
* Header file for merge sort
***********************************************/
#ifndef MERGESORT_H
#define MERGESORT_H

#include "common.h"
/**************************************************************************
** FUNCTION merge
** \param[in]: A, B - pointers to an array
** \param[in]: sizeA, sizeB - size of A and B, respectively
** \param[inout]: output - pointer to output array
**                       - size = sizeA + sizeB
** RETURN: n/a
***************************************************************************/
void merge( IN int* A,
            IN int* B,
            IN int sizeA,
            IN int sizeB,
            INOUT int* output );

/**************************************************************************
** FUNCTION mergeSort
** \param[inout]: A - pointer to an array of integers
**                  - outputs sorted array
** \param[in]: left  - index to start sorting at
** \param[in]: right - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void mergeSort( INOUT int* A,
                IN int left,
                IN int right );
#endif