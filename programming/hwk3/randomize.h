/**********************************************
* File: randomize.h
* Header file for random permutation functions
***********************************************/
#ifndef RANDOMIZE_H
#define RANDOMIZE_H

#include "common.h"

/**************************************************************************
** FUNCTION randomizeInPlace
** \param[inout]: data - on input: pointer to an array of ints with
**                       integers
**                     - on output: pointer to an array of ints with
**                       integers in the range [0,size-1] in random order
** \param[in]: size - size of data
** RETURN: n/a
** This algorithm was taken from Introduction to Algorithms 3e by Cormen,
** Leiserson, Rivest, and Stein.
***************************************************************************/
void randomizeInPlace( INOUT int* data,
                       IN size_t size );

#endif