/**********************************************
* File: mergeSort.c
* Implementation file for merge sort
***********************************************/
#include "mergeSort.h"

/**************************************************************************
** FUNCTION merge
** \param[in]: A, B - pointers to an array
** \param[in]: sizeA, sizeB - size of A and B, respectively
** \param[inout]: output - pointer to output array
**                       - size = sizeA + sizeB
** RETURN: n/a
***************************************************************************/
void merge( IN int* A,
            IN int* B,
            IN int sizeA,
            IN int sizeB,
            INOUT int* output )
{
   int i = 0, j = 0;
   
   // actual merge operation
   while(i < sizeA && j < sizeB) {
      if( A[i] < B[j] ) {
         output[i+j] = A[i];
         i++;
      } else {
         output[i+j] = B[j];
         j++;
      }
   }
   
   // we still have to finish one of the two arrays
   // just use this to figure out which one
   for( ; i < sizeA; i++ ) {
      output[i+j] = A[i];
   }
   for( ; j < sizeB; j++ ) {
      output[i+j] = B[j];
   }
}

/**************************************************************************
** FUNCTION mergeSort
** \param[inout]: A - pointer to an array of integers
**                  - outputs sorted array
** \param[in]: left  - index to start sorting at
** \param[in]: right - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void mergeSort( INOUT int* A,
                IN int left,
                IN int right )
{
   int* tmp;

   if( left >= right ) return;
   mergeSort( A, left, (right+left-1)/2 );
   mergeSort( A, (right+left+1)/2, right );

   tmp = (int *) malloc((right-left+1)*sizeof(int));
   merge( &A[left], &A[(right+left+1)/2], (right-left+1)/2, (right-left+2)/2, tmp);
   memcpy( &A[left], tmp, (right-left+1)*sizeof(int) );
   free(tmp);
   tmp = NULL;
}
