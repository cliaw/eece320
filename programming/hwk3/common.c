#include "common.h"

void swap( INOUT int* x,
           INOUT int* y )
{
   int tmp = *x;
   *x = *y;
   *y = tmp;
}