/**********************************************
* File: quickSort.h
* Header file for quick sort
***********************************************/
#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "common.h"

/**************************************************************************
** FUNCTION partition
** \param[inout]: A - pointer to an array of integers
**                  - outputs partitioned array
** \param[in]: left  - index to start partitioning at
** \param[in]: right - index to stop partitioning at
** RETURN: index of pivot in partitioned array
** The function randomly picks a pivot point. It will create a new array
** to store the partitioned array.
***************************************************************************/
int partition( INOUT int* A,
               IN int left,
               IN int right );

/**************************************************************************
** FUNCTION quickSort
** \param[inout]: A - pointer to an array of integers
**                  - outputs sorted array
** \param[in]: left  - index to start sorting at
** \param[in]: right - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void quickSort( INOUT int* A,
                IN int left,
                IN int right );

#endif
