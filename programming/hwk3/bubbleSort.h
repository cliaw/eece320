/**********************************************
* File: bubbleSort.h
* Header file for bubble sort
***********************************************/
#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include "common.h"

/**************************************************************************
** FUNCTION bubbleSort
** \param[inout]: data - pointer to an array of integers
**                     - outputs sorted array
** \param[in]: start - index to start sorting at
** \param[in]: end - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void bubbleSort( INOUT int* data,
                 IN int start,
                 IN int end );

#endif