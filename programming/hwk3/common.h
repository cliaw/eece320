/*******************************
* File: common.h
* Some basic macros and defines
*******************************/

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define DBG 0

/*********************************
* Define for clarity in functions
*********************************/
#define IN
#define OUT
#define INOUT

// What have we implemented so far??
#define HAVE_BUBBLESORT_H 1
#define HAVE_MERGESORT_H 1
#define HAVE_QUICKSORT_H 1

#define min(a,b) (a < b ? a : b)
#define max(a,b) (a > b ? a : b)

typedef enum {
   FALSE,
   TRUE
} bool_t;

// swapping
#define FASTSWAP 0
void swap( INOUT int *x,
           INOUT int *y );

#endif
