/**********************************************
* File: quickSort.c
* Implementation file for quick sort
***********************************************/
#include "quickSort.h"

/**************************************************************************
** FUNCTION partition
** \param[inout]: A - pointer to an array of integers
**                  - outputs partitioned array
** \param[in]: left  - index to start partitioning at
** \param[in]: right - index to stop partitioning at
** RETURN: index of pivot in partitioned array
** The function randomly picks a pivot point. It will create a new array
** to store the partitioned array.
***************************************************************************/
int partition( INOUT int* A,
               IN int left,
               IN int right )
{
   int pivot;
   int *tmp = NULL;
   int i = 0, j = right-left, k = 0;

   tmp = (int *) malloc( (right - left + 1) * sizeof(int) );
   assert( tmp );
   memset( tmp, 0, (right-left+1)*sizeof(int) );
   pivot = rand() % (right-left);
   pivot += left;

   // if > pivot, start filling in from the right
   // if < pivot, start filling in from the left
   for( k = left; k <= right; k++ ) {
      if( k == pivot ) continue;
      if( A[k] < A[pivot] ) tmp[i++] = A[k];
      else tmp[j--] = A[k];
   }
   
   // one last element to put into tmp.. the pivot!
   tmp[i] = A[pivot];
   memcpy( &(A[left]), tmp, (right-left+1)*sizeof(int) );
   free(tmp);
   tmp = NULL;

   // i+left is the location of the pivot in the new array
   return (i+left);
}

/**************************************************************************
** FUNCTION quickSort
** \param[inout]: A - pointer to an array of integers
**                  - outputs sorted array
** \param[in]: left  - index to start sorting at
** \param[in]: right - index to stop sorting at
** RETURN: n/a
***************************************************************************/
void quickSort( INOUT int* A,
                IN int left,
                IN int right )
{
   int pivot;

   if( left >= right ) return;
   pivot = partition(A, left, right );
   quickSort(A, left, pivot - 1);
   quickSort(A, pivot + 1, right );
}

