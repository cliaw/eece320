%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All the content is in one file because I do not expect multiple editors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% packages
\usepackage{centernot}
\usepackage{latexsym}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[T1]{fontenc}
\usepackage{mathptmx}
\usepackage{palatino}
%\usepackage{times}
\usepackage{amsmath, amssymb}
\usepackage{fullpage}
\usepackage{complexity}
\usepackage{hyphenat}
\usepackage{multirow}
\usepackage{moreverb}
\usepackage[usenames,dvipsnames]{color}
\usepackage{boxedminipage}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{setspace}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes}
\lstset{language=C,
        basicstyle=\footnotesize,
        numbers=left,
        numberstyle=\footnotesize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% basic definitions, Counting-notes.tex

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}{Definition}
\newtheorem{property}{Property}
\newtheorem{observation}{Observation}
\newtheorem{remark}{Remark}
\newtheorem{claim}{Claim}

\newenvironment{proof}
        {\noindent {\em Proof.}~~~} %\\
        {\begin{flushright}$\Box$\end{flushright}}

\newenvironment{indent1}
    {\begin{list}{}{
        \setlength{\leftmargin}{20mm}
        \setlength{\labelwidth}{15mm}
        \setlength{\labelsep}{5mm}}}
    {\end{list}}

\addtolength{\parskip}{0.1in}
%\addtolength{\oddsidemargin}{-0.25in}
%\addtolength{\evensidemargin}{-0.25in}
%\addtolength{\textwidth}{0.5in}
%\addtolength{\topmargin}{-.25in}
%\addtolength{\textheight}{0.75in}	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% title details

\title{
	Problem Set 8\\
        \scriptsize{\textsection
                    \ Christopher Liaw, 73118093 - Nicholas Fischer, 77157097 - Lenny Mah, 70630090
                    \textsection }
	\\ \vspace*{0.2in} \hrule
}
%\author{
%Christopher Liaw, 73118093
%\hrule
%}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\setlength{\baselineskip}{1\baselineskip}

\maketitle

\vspace*{-0.7in}

\begin{enumerate}
\item (Matrix multiplication.)
\begin{enumerate}[(a)]
\item Using naive matrix multiplication, computing $M = M_1(M_2M_3)$ costs $n_1n_2n_4 + n_2n_3n_4$ multiplications, whereas computing $M = (M_1M_2)M_3$ costs $n_1n_2n_3 + n_1n_3n_4$ multiplications.
If $n_1 = n_2 = n_3 = 1$ and $n_4 = 2$, then the latter is more efficient.
As a concrete example, consider $M_1 = 2$, $M_2 = 2$, and $M_3 = (1,3)$.
The former requires 4 multiplications, whereas the latter requires 3.
\item Suppose we wish to multiply $n$ matrices together.
If we know the minimum cost of multiplying matrices $M_2$ to $M_{i-1}$ and $M_i$ to $M_n$, where $3 \le i \le n$, then we can exhaustively check the cost of all multiplications of the form $M_1\prod_{j = 2}^{i-1}M_j\prod_{j = i}^{n} M_j$.
This leads to the following recursive formulation to multiply matrices $m_1$ to $m_2$ together:
\[
c_{m_1,m_2} = \min_{m_1 < i \le m_2} \{ c_{m_1+1,i-1} + c_{i,m_2} + n_{m_1}n_{m_1+1}n_{i} + n_in_{i+1}n_{m_2+1} \},
\]
where $c_{i,j}$ is the cost of multiplying matrices $i$ through $j$ and $n_i$ is the number of rows of matrix $i$ or the number of columns of matrix $i+1$.
Algorithmically, we can generate an $n\times n$ matrix $C[1..n][1..n]$, where $C[i][j]$ is the cost of multiplying matrices $i$ through $j$.
If $i \ge j$, then we can set $C[i][j] = 0$ to indicate there is nothing to multiply or that multiplication of matrices is not commutative.
With this memoization in mind, we can implement the following:
\[
C[m_1][m_2] = \left\{
\begin{array}{ll}
0 & \mbox{if } i \ge j \\
\min_{m_1 < i \le m_2} \left\{
\begin{array}{l}
C[m_1+1][i-1] + C[i][m_2] + \\
\delta_{m_1+1,i}n_{m_1}n_{m_1+1}n_{i} + \delta_{i,m_2}n_in_{i+1}n_{m_2+1} 
\end{array}
\right\} & \mbox{otherwise}
\end{array}
\right.,
\]
where,
\[
\delta_{i,j} = \left\{
\begin{array}{ll}
0 & \mbox{if } i = j\\
1 & \mbox{otherwise}
\end{array}
\right..
\]
\begin{theorem}
$C[m_1][m_2]$ calculates the optimal cost of multiplying matrices $m_1$ through $m_2$.
\end{theorem}
\begin{proof}
The proof is by induction.
For the base case, we have one $1 \times 1$ matrix.
This returns 0, as expected.

Assuming we have at least $k$ matrices, suppose the theorem remains true for $C[1..m_1-1][1..m_2-1]$ and $C[m_1][m_2+1..k]$.
If $m_1 \ge m_2$, then we set it to 0, since multiplication of matrices is not commutative.
So, suppose $m_1 < m_2$.
The calculation of $C[m_1][m_2]$ partitions matrices $m_1 + 1$ through $m_2$ into two groups, $m_1 + 1$ through $m_1+i-1$ and $m_1 + i$ through $m_2$.
By our induction hypothesis, the calculation of the cost for both subgroup of matrices is optimal and is obtained from $C[m_1+1][i-1]$ and $C[i][m_2]$.
The cost is then the cost of multiplying through each matrix in each partition, as well as multiplying $M_1$ with the result of the first partition and then with the result of the second partition.
Hence, the total cost is $C[m_1+1][i-1] + C[i][m_2] + \delta_{m_1+1,i}n_{m_1}n_{m_1+1}n_{i} + \delta_{i,m_2}n_in_{i+1}n_{m_2+1}$ where the $\delta_{m_1+1,i}$ and $\delta_{i,m_2}$ terms are there to catch the cases when the first partition or the second partition is empty, respectively.

Using this method to calculate the cost of a sequence of matrix multiplications, we note that we have exhaustively checked all possibilities and taken the minimum possible value.
Hence, $C[m_1][m_2]$ calculates for us, the optimal cost.
\end{proof}

The time to calculate each entry in $C$ takes at most linear time and there are a quadratic number of entries in $C$ to fill.
Hence, the running time is $\Theta(n^3)$.
\end{enumerate}
\item (Shortest paths with few hops.)
As motivation for the discussion below, we first note that Dijkstra's algorithm works because the edge weights are elements of an ordered set such as $\mathbb{R}$.

Define the set $W = \mathbb{R} \times \mathbb{Z}$ to be the set of ordered pairs $(c, n)$, where $c \in \mathbb{R}$ and $n \in \mathbb{Z}$.
The elements of $W$ will be used to represent both the cost and the number of edges traversed in Dijkstra's algorithm.
For $w_1,\ldots,w_k \in W$, define $\sum_{i = 1}^k w_i = (\sum_{i=1}^k c_i, \sum_{i=1}^k n_i)$.
$W$ is clearly closed under addition.
For $w_i, w_j \in W$, define $w_i < w_j$ if and only if $c_i < c_j \vee (c_i = c_j \wedge n_i < n_j)$ (dictionary order).
This defines an order suitable for use in Dijkstra's algorithm.

Redefine the edge weights as $w_{ij} = (c_{ij},1)$, where $c_{ij}$ is the cost (as discussed in class) of going from vertex, $v_i$, to vertex, $v_j$.
With this redefinition, Dijkstra's algorithm can be applied directly, without modification.
The running time remains the same, as $O(|E| + |V|\log|V|)$.

\begin{remark}
If $s$ is the source and $t$ is the destination, then $d(t) = (c,n)$ is the minimal weight among all possible paths from $s$ to $t$.
This means that for any other path, either the cost, $c$, is greater, or if the costs are the same, then the number of edges traversed is minimal for the same $c$.
\end{remark}

\item (A little bit of Number Theory.)
If we show the contrapositive is true, we are done.
Suppose $n = 2$. Then $a^2$ must be odd, so $a$ is odd.
The conclusion follows since the remainder of $a / 2$ is always $1$ when $a$ is odd.

Suppose $n > 2$ and $n$ is prime.
Since $a^2 \equiv 1 \mod n$, $n|(a^2-1)$.
Since $n$ is prime, it divides only one of $a-1$ or $a+1$, so that $a-1 \equiv 0 \mod n \wedge a+1 \equiv 2 \mod n$ or $a-1 \equiv -2 \mod n \wedge a+1 \equiv 0 \mod n$.
Rearranging gives, $a \equiv \pm 1 \mod n$.
Therefore, if $n$ is prime, then all square roots of 1 modulo $n$ are trivial.

\item (Hashing and probabilities.)
Denote $X_i$ be the random variable for the number of balls in bin $i$.
For some $k \le n$, we have
\[
\begin{array}{ll}
Pr[X_i \ge k] & = \binom{n}{k} \left(\frac{1}{n}\right)^k \left(1\right)^{n-k}\\
& = \frac{n!}{(n-k)!k!} \frac{1}{n^k} \\
& \le \frac{n^k}{k!}\frac{1}{n^k} \\
& \le \left(\frac{e}{k}\right)^k,
\end{array}
\]
where the $1^{n-k}$ term in the first line indicates that we do not care which of the $n$ bins that all but $k$ balls end up in.
Put $k = \frac{3\ln n}{\ln \ln n}$.
Then:
\[
\begin{array}{ll}
Pr[X_i \ge \frac{3 \ln n}{\ln \ln n}] & = \left( \frac{e\ln\ln n}{3\ln n} \right) ^ {\frac{3 \ln n}{\ln \ln n}}\\
& = \exp\left(  \frac{3\ln n}{\ln \ln n} \left( 1+\ln\ln\ln n - \ln 3 - \ln \ln n \right)  \right) \\
& \le \exp\left(  \frac{3\ln n\ln\ln\ln n}{\ln \ln n} - 3\ln n  \right).
\end{array}
\]
Using the fact that $\ln n = O(n)$ and replacing $n$ with $\ln \ln n$ gives, for $n$ sufficiently large
\[
\begin{array}{ll}
Pr[X_i \ge \frac{3 \ln n}{\ln \ln n}] & \le \exp\left( \ln n - 3 \ln n \right) \\
& = \frac{1}{n^2}.
\end{array}
\]
Finally, if $E_i$ is the event that $X_i \ge \frac{3 \ln n}{\ln \ln n}$ then,
\[
Pr\left(\bigcup_{i=1}^n E_i\right) \le \frac{n}{n^2} = \frac{1}{n}.
\]
\end{enumerate}
\end{document}
