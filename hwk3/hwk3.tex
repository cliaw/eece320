%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All the content is in one file because I do not expect multiple editors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,A4]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% packages
\usepackage{latexsym}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[T1]{fontenc}
\usepackage{mathptmx}
\usepackage{palatino}
%\usepackage{times}
\usepackage{amsmath, amssymb}
\usepackage{fullpage}
\usepackage{complexity}
\usepackage{hyphenat}
\usepackage{multirow}
\usepackage{moreverb}
\usepackage[usenames,dvipsnames]{color}
\usepackage{boxedminipage}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{setspace}

\lstset{language=C,
        basicstyle=\footnotesize,
        numbers=left,
        numberstyle=\footnotesize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% basic definitions, Counting-notes.tex

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}{Definition}
\newtheorem{property}{Property}
\newtheorem{observation}{Observation}
\newtheorem{remark}{Remark}
\newtheorem{claim}{Claim}

\newenvironment{proof}
        {\noindent {\em Proof.}~~~} %\\
        {\begin{flushright}$\Box$\end{flushright}}

\newenvironment{indent1}
    {\begin{list}{}{
        \setlength{\leftmargin}{20mm}
        \setlength{\labelwidth}{15mm}
        \setlength{\labelsep}{5mm}}}
    {\end{list}}

\addtolength{\parskip}{0.1in}
%\addtolength{\oddsidemargin}{-0.25in}
%\addtolength{\evensidemargin}{-0.25in}
%\addtolength{\textwidth}{0.5in}
%\addtolength{\topmargin}{-.25in}
%\addtolength{\textheight}{0.75in}	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% title details

\title{
	Problem Set 3\\
        \scriptsize{\textsection
                    \ Christopher Liaw, 73118093 - Nicholas Fischer, 77157097 - Lenny Mah, 70630090
                    \textsection }
	\\ \vspace*{0.2in} \hrule
}
%\author{
%Christopher Liaw, 73118093
%\hrule
%}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\setlength{\baselineskip}{1\baselineskip}

\maketitle

\vspace*{-0.7in}

\begin{enumerate}

\item (Recurrences.)
For this question, we use the master theorem as stated in \textit{Introduction to Algorithms 3e.} by Cormen, Leiserson, Rivest, and Stein.
\begin{theorem}
Let $T(n)$ be define by the recurrence
\[
T(n) = aT(n/b) + f(n).
\]
Let $\epsilon > 0$ be given. Then we have three cases.
\begin{enumerate}
\item If $f(n) = O\left( n^{n\log_ba - \epsilon} \right)$, then $T(n) = \Theta\left( n^{\log_ba} \right)$.
\item If $f(n) = \Theta\left( n^{\log_ba} \right)$, then $T(n) = \Theta\left( n^{log_ba}\lg n\right)$.
\item If $f(n) = \Omega\left( n^{\log_ba + \epsilon} \right)$ and if $af(n/b) \le cf(n)$ for some constant $c < 1$ and all $n$ sufficiently large, then $T(n) = \Theta(f(n))$.
\end{enumerate}
\end{theorem}
We will use the master theorem is (a), (b), (c), and (e).
\begin{enumerate}[(a)]
\item This is case 1 of the master theorem since $f(n) = n \in O\left( n^{\log_5{7}-\epsilon} \right)$.
Therefore, $T(n) \in \Theta \left( n^{\log_5{7}} \right) $.
\item This is case 1 of the master theorem since $f(n) = (\log n)^2 \in O \left( n^{\log_4{3}-\epsilon} \right)$.
Therefore, $T(n) \in \Theta \left( n^{\log_4{3}} \right) $.
\item The $\sqrt{n}$ is annoying, so we make the substitution $n = 2^k$ ($\sqrt{n} = 2^{\frac{k}{2}}$). The recurrence becomes $T(k) = T\left( \frac{k}{2}\right) + 4 = T\left( \frac{k}{2}\right) + \Theta(1)$.
This is case 2 of the master theorem, but it is also the recurrence for binary search.
In terms of $k$, we have $T(k) \in \Theta\left( \lg k \right)$.
Therefore, $T(n) \in \Theta\left( \lg\lg n \right)$.
\item We cannot use the master theorem for this. But note that $T(n) = T(n-2) + O(n) = T(n-4) + O(n-2) + O(n) = \ldots$.
Unrolling the recurrence, we get:
\begin{eqnarray*}
T(n) & \le & \sum_{i=0}^{\frac{n}{2}} c(n-2i) \\
& = & c\left( \frac{n^2}{2} - \frac{n^2}{4} - \frac{n}{2}\right),
\end{eqnarray*}
where $c$ is a positive real constant.
So, $T(n) \in O(n^2)$.
\item This is case 3 of the master theorem, since $f(n) = n^{3/2}\lg n = n^{\log_{16}{64}}\lg n \in \Omega\left( n^{\log_{16}{36}} \right)$ and $36\left( \frac{n}{16} \right) ^{\frac{3}{2}} \lg{\frac{n}{16}} \le kn^{\frac{3}{2}}\lg n$, for $n, k$ sufficiently large. For example, any $n > 10^{10}$ for $k > 10^{100}$ will be sufficient.
So, $T(n) \in \Theta\left( n^{\frac{3}{2}}\lg n \right)$.
\item We have to unravel this recurrence. Note that $T(n) = T(n-1) + k^n = T(n-2) + k^{n-1} + k^n = \ldots$.
So,
\begin{eqnarray*}
T(n) & \le & \sum_{i=0}^{n} k^n \\
& = & c\frac{k^{n+1}-1}{k-1},
\end{eqnarray*}
where $c$ is a positive real constant.
So, $T(n) \in \Theta(k^n)$.
\end{enumerate}

\begin{algorithm}\label{alg1}
\begin{algorithmic}[1]
\STATE MergeSort on $A[\lfloor \sqrt{n} \rfloor .. n]$
\STATE Merge the arrays $A[1..\lfloor \sqrt{n} \rfloor-1]$ and $A[\lfloor \sqrt{n} \rfloor..n]$
\end{algorithmic}
\caption{$\mbox{\sc Sort}(A)$}
\end{algorithm}

\item (Partially sorted lists.) We can do this in $\Theta(n)$.
This is optimal since insertion into a sorted list is bounded by $O(n)$.

Our algorithm consists of two steps:
\begin{enumerate}[(i)]
\item Run MergeSort on the unsorted array.
\item Merge the two sorted arrays.
\end{enumerate}

The pseudo-code is shown in Algorithm 1.

\textbf{Correctness.} We have already justified MergeSort in class.
Line 1 will result in two sorted arrays (since the first is already sorted).
Line 2 will simply merge the two arrays.

\textbf{Asymptotic Complexity.} For line 1, we are sorting an array of size $\sqrt{n}$.
So, this is bounded by $O(\sqrt{n}\lg n)$.
Line 2 merges the two arrays.
We have seen in class that this operations takes $\Theta(n-\sqrt{n} + \sqrt{n}) = \Theta(n)$.
Hence, the running time of this algorithm is $\Theta(n)$.

\item \textcolor{Red}{} We define the weight density as:
\[
\rho = \frac{w}{t},
\]
where $w$ is the weight of the request and $t$ is the time taken to perform the request.
Let $P$ be an ordered (not necessarily sorted) set of all weight densities for a particular day.
We claim that the optimal solution occurs when we sort $P$ and select the request with the largest weight density first.
Hence, we propose Algorithm 2.\\
\begin{algorithm}[ht]\label{alg2}
\begin{algorithmic}[1]
\STATE sort $P [1..n]$ by weight, such that $\rho_1 \ge \rho_2 \ge \ldots \ge \rho_n$
\STATE schedule $\leftarrow$ empty
\FOR{ $i \leftarrow 1$ to $n$ }
	\STATE add request, $r_i$, with associated weight density, $\rho_i$, to end of schedule
\ENDFOR
\end{algorithmic}
\caption{$\mbox{\sc Scheduling}(P)$}
\end{algorithm}
\textbf{Proof of correctness.} We will prove one lemma first before proving that our algorithm is optimal.
\begin{definition}
We say that there exists an inversion in the scheduling if there is some $\rho_j,\rho_k$ such that $\rho_j > \rho_k$ for $j > k$.
\end{definition}
\begin{lemma}\label{lemma:q3}
Let $S = \sum_{i=i}^n w_iC_i$. Suppose there is a single pair of weight densities, $\rho_j, \rho_k$, which are inverted, so that $\rho_j > \rho_k$ and $j > k$.
Then there exists a sum, $S'$, such that $S'$ does not have the inversion and $S' < S$.
\end{lemma}
\begin{proof}
Note that for each term, $w_iC_i$, in the objective function, we can write $w_iC_i=\rho_i t_i \sum_{j=1}^i t_j$.
Expanding the sum for $S$ and $S'$ gives:
\begin{eqnarray*}
S & = & \rho_1 t_1t_1 + \rho_1 t_2(t_1 + t_2) + \ldots + \rho_k t_k(t_1 + \ldots t_k) + \ldots + \rho_j t_j(t_1 + \ldots t_k + \ldots t_j) + \ldots \\
S' & = & \rho_1 t_1t_1 + \rho_1 t_2(t_1 + t_2) + \ldots + \rho_j t_j(t_1 + \ldots t_j) + \ldots + \rho_k t_k(t_1 + \ldots t_j + \ldots t_k) + \ldots
\end{eqnarray*}
We can assume there are $n$ terms in the sum between $j$ and $k$, which we will label $i_{1}$ to $i_n$.
Now, we wish to take the difference.
Notice that the terms $\rho_kt_kt_k$ and $\rho_jt_jt_j$ will cancel since they appear in both sums.
In addition, all terms with indices not between $j$ and $k$ will cancel.
Finally, if $m \in [1,n]$, then
\[
\rho_{i_m}t_{i_m}(t_1 + \ldots + t_k + \ldots + t_{i_m}) - \rho_{i_m}t_{i_m}(t_1 + \ldots + t_j + \ldots + t_{i_m}) = \rho_{i_m}t_{i_m}(t_k - t_j).
\]
So,
\begin{eqnarray*}
S - S' & = & \rho_jt_jt_k + \rho_j t_j \sum_{m=1}^n t_{i_m} + \sum_{m=1}^n (t_k-t_j)\rho_{i_m}t_{i_m}-\rho_kt_k\sum_{m=1}^n t_{i_m} - \rho_kt_jt_k\\
& = & t_j\sum_{m=1}^n t_{i_m}(\rho_j-\rho_{i_m}) - t_k\sum_{m=1}^n t_{i_m}(\rho_k-\rho_{i_m}) + t_kt_j(\rho_j - \rho_k).
\end{eqnarray*}
Note that if there are no inversions, then $\rho_j \ge \rho_{i_m} \ge \rho_{k}$.
So, $\sum_{m=1}^n t_{i_m}(\rho_j-\rho_{i_m}) \ge 0$, $\sum_{m=1}^n t_{i_m}(\rho_k-\rho_{i_m}) \le 0$ and $\rho_j - \rho_k > 0$.
Hence $S - S' > 0$. Rearranging gives $S' < S$, which proves the lemma.
\end{proof}
\begin{corollary}\label{cor:q3}
A schedule without any inversions is always better than a schedule with inversions.
\end{corollary}
\begin{proof}
We can repeatedly apply Lemma \ref{lemma:q3} until there are no more inversions.
\end{proof}
\begin{theorem}\label{thm:q3}
The greedy algorithm is optimal.
\end{theorem}
\begin{proof}
Suppose the greedy algorithm is not optimal and there exists an optimal algorithm better than our greedy algorithm.
Since our algorithm has no inversions, the new algorithm must contain at least one inversion.
Otherwise, the new algorithm will have the same objective function as our algorithm.
By Corollary \ref{cor:q3}, there is an algorithm better than the optimal algorithm, but this is a contradiction.
Hence, the greedy algorithm is optimal.
\end{proof}

\textbf{Asymptotic complexity.} Scheduling takes linear time and sorting takes $\Theta(n \lg n)$ time.
Hence, the running time of this algorithm is $\Theta(n \lg n) + \Theta(n) = \Theta(n \lg n)$.

\item At a high level, our algorithm looks for the booking that ends the earliest. Using this booking, it searches for another booking, booking A, that overlaps with our original booking at some point and ends the latest. Our algorithm then searches for the booking that ends the earliest after booking A (without considering any bookings that started before booking A ends). This process is then repeated.

The details is provided in Algorithm 3.
\begin{algorithm}[ht]\label{alg3}
\begin{algorithmic}[1]
\STATE copy $B$ to two lists, $B_{s}$ and $B_e$.
\STATE sort $B_s$ by starting time
\STATE sort $B_e$ by ending time
\STATE $i_e \leftarrow 1, i_s \leftarrow 1$
\STATE proctorList $\leftarrow$ empty
\WHILE{ $i_e < B_e.length()$ }
	\STATE tmp $\leftarrow$ index of $b_s \in B_s[i_s..B_s.length()]$ with largest start time such that $b_s.start() \le B_e[i_e].end()$ (binary search)
	\STATE nextProctor $\leftarrow B_s[i_s]$
	\FOR{ $k \leftarrow i_s$ to tmp }
		\IF{ nextProctor.$end() < B_s[k].end()$ }
			\STATE nextProctor $\leftarrow B_s[k]$
		\ENDIF
	\ENDFOR	
	\STATE add nextProctor to proctorList
	\STATE $i_s \leftarrow$ tmp + 1
	\STATE $i_e \leftarrow$ index of $b_e \in B_e[i_e+1..B_e.length()]$ with smallest end time, such that $b_e.end() > B_s[i_s].end()$ or $+\infty$, if none exists (binary search)
\ENDWHILE
\RETURN proctorList
\end{algorithmic}
\caption{$\mbox{\sc Booking}(B)$}
\end{algorithm}
Note that proctorList will contain the names and the booking time of each proctor.

The proof of correctness is given below.
\begin{theorem}
The algorithm, $\mbox{\sc Booking}$, terminates.
\end{theorem}
\begin{proof}
After each iteration, $i_e$ increments at least once by one.
Eventually, $i_e \ge B_e.length()$.
Hence, the algorithm terminates.
\end{proof}
\begin{theorem}
In the algorithm, $\mbox{\sc Booking}$, all bookings have at least one proctor.
\end{theorem}
\begin{proof}
The invariant that we will use is that at the end of the each iteration of the loop, all bookings in $B_s[1..i_s]$ have at least one proctor.\\
\textbf{Initialization.} To pick the first proctor, the algorithm looks at the booking that ends the earliest. This is the first element in the list $B_e$.
Then, from the room bookings that overlap with this booking, the booking that ends the latest is selected.
Line 16 ensures that the start time for the next proctor begins after the first proctor has already left.
In addition, the proctor arrives before the end of the first booking.
So, all bookings that start before the proctor leaves have at least one proctor.
This is the set of bookings in $B_s[1..i_s-1]$.\\
\textbf{Maintenance.} Suppose the invariant holds for several iterations.
When the algorithm enters the loop again, all bookings in $B_s[1..i_s-1]$ have at least one proctor, by the invariant.
Line 16 of the previous iteration and line 7 of the current iteration guarantees that the new proctor that is chosen will start after the previous proctor leaves, but before the end of any of the bookings that began after the previous proctor left.
The new proctor selected will have the booking that ends the latest.
Hence, all bookings that begin before this new proctor leaves will have at least one proctor and the invariant holds.
\end{proof}
\begin{theorem}
The algorithm, $\mbox{\sc Booking}$, is optimal.
\end{theorem}
\begin{proof}
Suppose the algorithm is not optimal.
Then there exists an algorithm, AlgB, that does better.
Suppose that the first $i-1$ proctors chosen by both algorithms are the same and that they differ on the $i$th proctor.
Suppose that the previous proctor left at time, $t$.
Our algorithm will choose a proctor that leaves as late as possible while being able to supervise all bookings after $t$ and before he leaves.
Let $t_1$ be the time that this proctor leaves.
If AlgB chooses a proctor that leaves later and still meets the criteria, then our algorithm would have chosen it.
Hence, AlgB must choose a proctor that leaves before $t_1$.
Let $t_2$ be the time that AlgB's proctor leaves.
AlgB must choose another proctor for bookings that begin between $t_2$ and $t_1$.
Hence AlgB is not better than our algorithm for this sub-problem and contradicts the fact that it is optimal.
\end{proof}

\textbf{Asymptotic complexity.} We claim that the running time of the algorithm is $\Theta(n \lg n)$.
The time taken to sort two lists is $\Theta(n \lg n)$.
Line 7 and line 16 take $O(\lg n)$ in each iteration and runs at most $n$ times, so takes $O(n \lg n)$ time.

For lines 9 to 13, $k$ runs from tmp to $i_s$.
Before the loop, tmp is at least $i_s$.
After the loop, at line 15, $i_s$ is at least tmp.
In the worst case, the algorithm looks at each element in $B_s$ at most once.
Hence, this is $O(n)$.

Therefore, the running time is $\Theta(n \lg n) + O(n \lg n) + O(n) = \Theta(n \lg n)$.
\end{enumerate}

\end{document}
